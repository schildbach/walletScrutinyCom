---
layout: archive
title: "Donate"
permalink: /donate/
---


![QR-Code for mobile](/images/qr.png)

If you like what we are doing, you can donate to
<a href="bitcoin:bc1qkatcx9sxd6257cmyw338w96vw63ak7xsc2x43c?label=WalletScrutiny.com" alt="send coins">bc1qkatcx9sxd6257cmyw338w96vw63ak7xsc2x43c</a>.

Our dream is to see all wallets be verifiable and to also get them verified
thanks to bug bounties that are worth the security researchers' time. To read
more on our vision, read our [methodology](/methodology/).

While
building what you see so far, we had many ideas on how to substantially reduce
the risk for mobile wallet users and we would love to implement them.
