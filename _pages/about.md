---
permalink: /
title: "Is your wallet secure?"
excerpt: "Many wallets are not open to public scrutiny"
author_profile: true
redirect_from:
  - /about/
  - /about.html
---


![hacker](/images/hacker.jpg)

**Wallet Scrutiny** is a project aimed at improving the security of
Android Bitcoin Wallets.


What protects your Bitcoins?
============================

![digital fortress by driver Photographer https://www.flickr.com/photos/72334647@N03/44546614212/in/photostream/](/images/fortress.jpg)

Do you own your Bitcoins or do you trust that your app allows you to use "your"
coins while they are actually controlled by "them"? Do you have a backup? Do
"they" have a copy they didn't tell you about? Did anybody check the wallet for deliberate backdoors
or vulnerabilities? Could anybody check the wallet for those?

We try to answer these questions for Bitcoin wallets on Android.

Read about [our methodology](/methodology/) to understand in more detail or just
see what we have to say about your wallet:

{% include list_of_wallets.html %}

How many users are in each category?
====================================

The following grid shows wallets by download count and category:

{% include grid_of_wallets.html %}
