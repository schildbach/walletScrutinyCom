---
title: "Coinomi Wallet :: Bitcoin Ethereum Altcoins Tokens"
altTitle: 

users: 1000000
appId: com.coinomi.wallet
launchDate: 2014-01-31
latestUpdate: 2020-01-07
apkVersionName: "Varies with device"
stars: 4.5
ratings: 21150
reviews: 13578
size: Varies with device
website: https://www.coinomi.com/
repository: 
issue: 
icon: com.coinomi.wallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty
date: 2019-11-14
reviewStale: true
reviewArchive:


internalIssue: 
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/2019/11/coinomi/
redirect_from:
  - /coinomi/
  - /com.coinomi.wallet/
---


Coinomi Wallet :: Bitcoin Ethereum Altcoins Tokens
claims to be non-custodial but we cannot find any source code on their
[official GitHub page](https://github.com/coinomi/).

Our verdict: This app is **not verifiable**.


Further observations
--------------------

In terms of security issues, Coinomi had the worst kind of bug that luckily did
not lead to known financial losses:
*The 12 word backup was being sent to a remote server.*

Apparently the issue was only in the desktop version of the software. If yo want
to learn more, read Coinomi's
[official statement](https://medium.com/coinomi/official-statement-on-spell-check-findings-547ca348676b)
or search the web for
["coinomi security issues"](https://duckduckgo.com/?q=coinomi+security+issue) or
["coinomi spell checker"](https://duckduckgo.com/?q=coinomi+spell+checker) for
third party interpretations of the issue.
