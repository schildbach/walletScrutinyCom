---
title: "Crypto Watch Wallet-Track address balance and more"
altTitle: 

users: 1000
appId: com.codit.cryptowatchwallet
launchDate: 2018-02-28
latestUpdate: 2018-10-27
apkVersionName: "1.3"
stars: 4.3
ratings: 66
reviews: 39
size: 2.9M
website: undefined
repository: 
issue: 
icon: com.codit.cryptowatchwallet.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty
date: 2019-12-20
reviewStale: false
reviewArchive:


internalIssue: 72
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.codit.cryptowatchwallet/
redirect_from:
  - /com.codit.cryptowatchwallet/
---


This appears to be only a tracker for bitcoin addresses and not for actually
spending coins from it. Don't give it your private keys and you should be save.