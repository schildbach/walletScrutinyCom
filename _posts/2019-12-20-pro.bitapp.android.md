---
title: "PureWallet-CryptoWallet for BTC,BCH,ETH,TRON,IOST"
altTitle: 

users: 1000
appId: pro.bitapp.android
launchDate: 2018-11-20
latestUpdate: 2019-10-11
apkVersionName: "v2.6.0"
stars: 3.5
ratings: 21
reviews: 13
size: 31M
website: https://www.purewallet.org
repository: 
issue: 
icon: pro.bitapp.android.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty
date: 2019-12-30
reviewStale: false
reviewArchive:


internalIssue: 79
providerTwitter: PureWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/pro.bitapp.android/
redirect_from:
  - /pro.bitapp.android/
---


Their description claims:

> Guarantee your exclusive control over seed words or private key of the wallet.

which probably means non-custodial.

But we see no word about Open Source.

On their website we find a [link to GitHub](https://github.com/BitApp) but nothing
about Android there, so we assume it is closed-source.

Verdict: This app is **not verifiable**.